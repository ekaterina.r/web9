function animate(options) {

  let start = performance.now();

  requestAnimationFrame(function animate(time) {

    let timeFraction = (time - start) / options.duration;
    if (timeFraction > 30) timeFraction = 30;


    let progress = formingTiming(1.5, timeFraction);
    
    options.draw(progress);

    if (timeFraction < 30) {
      requestAnimationFrame(animate);
    }

  });
}

function formingTiming(x, timeFraction) {
  return Math.pow(timeFraction, 2) * ((x + 1) * timeFraction - 8*x)
}

function linear(timeFraction) {
  return timeFraction;
}
function show() { {
    form_popup.style.display = "inline";
  
    animate({
      duration: 200,
      timing: formingTiming(1.5, linear),
      draw(progress) {
        if(progress < 1) {form_popup.style.top = progress;}
        else {form_popup.style.top = 20 + '%';}
      }
    });
  };
}

function validate() {
        let a = document.forms["Myform"]["name"].value;
      if (a == "") {
        alert("Укажите ваше имя");
        return false;
      }
      let d = document.forms["Myform"]["phone"].value;
      if (d == "") {
        alert("Укажите ваш телефон");
        return false;
      }
      let x = document.forms["Myform"]["region"].selectedIndex;
      if (x == 0) {
        alert("Выберите регион");
        return false;
      }
      }
      $('#form').submit(function(e){
        e.preventDefault();
        let a;
        if (document.forms["Myform"]["name"].value != "" && document.forms["Myform"]["phone"].value !="" && document.forms["Myform"]["region"].selectedIndex !=0)
        $.ajax({
            type:"POST",
            url:"https://formcarry.com",
            data:$(this).serialize()
        })
        if (document.forms["Myform"]["name"].value != "" && document.forms["Myform"]["phone"].value !="" && document.forms["Myform"]["region"].selectedIndex !=0)
        {
            a=function done(){
            $(this).find("input").val("");
            alert("Спасибо за заявку! Мы скоро свяжемся с Вами!");
            $('#form').trigger("reset");
        } 
    }
        else {
           a=function fail(){
            alert("Что-то пошло не так");
        }
    }   
        a();
        return false;
      });

$('document').ready(function(){
    $('.knopochka').on('click', function(e){      
        e.preventDefault();
        let href = $(this).attr('href');
        getContent(href, true);
    });
    
    $('.close').click(function(){
         $('.pop_up').fadeOut();
         $('main').css('filter','none');
         window.history.back();
      });
});

window.addEventListener("popstate", function(e) {
    getContent(location.pathname, false);
});

function getContent(url, addEntry) {
        if(addEntry === true) {
		 window.requestAnimationFrame(show);
         $('.pop_up').fadeIn();
         $('main').css('filter','blur(5px)');
         window.history.pushState({page:1}, null, '?#form_popup');}
         else {
             $('.pop_up').fadeOut();
             $('main').css('filter','none');
             window.history.replaceState({page:0},null,'https://ekaterina.r.gitlab.io/web10');
         }
        }
